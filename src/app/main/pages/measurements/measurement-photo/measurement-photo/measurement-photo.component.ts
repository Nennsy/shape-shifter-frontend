import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-measurement-photo',
  templateUrl: './measurement-photo.component.html',
  styleUrls: ['./measurement-photo.component.scss']
})
export class MeasurementPhotoComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: { src: string }) { }
}
