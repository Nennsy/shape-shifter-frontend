import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasurementPhotoComponent } from './measurement-photo.component';

describe('MeasurementPhotoComponent', () => {
  let component: MeasurementPhotoComponent;
  let fixture: ComponentFixture<MeasurementPhotoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeasurementPhotoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MeasurementPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
