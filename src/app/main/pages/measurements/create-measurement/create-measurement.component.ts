//Angular
import {
  Component,
  OnInit,
  OnDestroy,
  Inject,
  Output,
  EventEmitter,
} from '@angular/core';
import { Subject, takeUntil, Observable, switchMap } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//Services
import { MeasurementsService } from '../services/measurements.service';
//Interface
import { MeasurementInterface } from '../models/measurement.interaface';
import { UploadPhotoInterface } from '../models/upload-photo.interface';
//Components
import { MeasurementsComponent } from '../measurements.component';

@Component({
  selector: 'app-create-measurement',
  templateUrl: './create-measurement.component.html',
  styleUrls: ['./create-measurement.component.scss'],
})
export class CreateMeasurementComponent implements OnInit, OnDestroy {
  @Output() onUpdateMeasurement: EventEmitter<MeasurementInterface> =
    new EventEmitter();
  measurementForm!: FormGroup;
  photoForm!: FormGroup;
  destroy$: Subject<boolean> = new Subject<boolean>();
  loading: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: MeasurementInterface | undefined,
    private measurementService: MeasurementsService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<MeasurementsComponent>
  ) { }

  ngOnInit(): void {
    if (this.data?._id) {
      this.configMeasurementForm(this.data);
    } else {
      this.configMeasurementForm(null);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  configMeasurementForm(measurementData: MeasurementInterface | null): void {
    this.photoForm = this.fb.group({
      file: [null, [Validators.required]],
      fileSource: [null, [Validators.required]],
    });
    this.measurementForm = this.fb.group({
      weight: [
        measurementData ? measurementData.weight : null,
        [Validators.required, Validators.min(5), Validators.max(5000)],
      ],
      chest: [
        measurementData ? measurementData.chest : null,
        [Validators.required, Validators.min(5), Validators.max(5000)],
      ],
      waist: [
        measurementData ? measurementData.waist : null,
        [Validators.required, Validators.min(5), Validators.max(5000)],
      ],
      hips: [
        measurementData ? measurementData.hips : null,
        [Validators.required, Validators.min(5), Validators.max(5000)],
      ],
      biceps: [
        measurementData ? measurementData.biceps : null,
        [Validators.required, Validators.min(5), Validators.max(5000)],
      ],
      date: [
        measurementData ? measurementData.date : null,
        [Validators.required],
      ],
    });
  }

  closeModal(flag: boolean): void {
    this.dialogRef.close(flag);
  }

  getUpdateSuccessfulHandler(): (
    updatedMeasurement: MeasurementInterface
  ) => void {
    return (updatedMeasurement: MeasurementInterface) => {
      this.onUpdateMeasurement.emit(updatedMeasurement);
      this.closeModal(true);
      this.measurementForm.reset();
      this.photoForm.reset();
    };
  }

  handleImageUpload(
    imgFormData: FormData,
    oldImgId?: string
  ): Observable<UploadPhotoInterface> {
    this.loading = true;
    return oldImgId
      ? this.measurementService
        .deletePhoto(oldImgId)
        .pipe(switchMap(() => this.measurementService.uploadPhoto(imgFormData)))
      : this.measurementService.uploadPhoto(imgFormData);
  }

  pipeImageMetaToMeasurementHandler(
    measurementID?: string
  ): (imgMeta: UploadPhotoInterface) => Observable<MeasurementInterface> {
    return (imgMeta: UploadPhotoInterface) => {
      const cfg = {
        photoUrl: imgMeta.url,
        photoId: imgMeta.public_id,
        ...this.measurementForm.value,
      };
      if (measurementID)
        return this.measurementService.updateMeasurement(measurementID, cfg);
      return this.measurementService.addMeasurement(cfg);
    };
  }

  submitMeasurement(): void {
    const fileSource = this.photoForm.get('fileSource')?.value;
    const fd = new FormData();
    fd.append('file', fileSource);
    const measurementID = this.data?._id;
    if (this.measurementForm.invalid) {
      return;
    }

    if (measurementID && fileSource) {
      this.handleImageUpload(fd, this.data?.photoId)
        .pipe(
          switchMap(this.pipeImageMetaToMeasurementHandler(measurementID)),
          takeUntil(this.destroy$)
        )
        .subscribe(this.getUpdateSuccessfulHandler());
      return;
    }

    if (measurementID && !fileSource) {
      this.measurementService
        .updateMeasurement(measurementID, {
          ...this.measurementForm.value,
        })
        .pipe(takeUntil(this.destroy$))
        .subscribe(this.getUpdateSuccessfulHandler());
      return;
    }

    if (!measurementID && this.photoForm.invalid) {
      this.photoForm.markAllAsTouched();
      return;
    }

    if (!measurementID) {
      this.handleImageUpload(fd)
        .pipe(
          switchMap(this.pipeImageMetaToMeasurementHandler()),
          takeUntil(this.destroy$)
        )
        .subscribe(this.getUpdateSuccessfulHandler());
    }
  }

  onPhotoSelected(event: Event): void {
    const element = event.target as HTMLInputElement;
    const selectedFile = <File>element.files![0];
    this.photoForm.patchValue({
      fileSource: selectedFile,
    });
  }
}
