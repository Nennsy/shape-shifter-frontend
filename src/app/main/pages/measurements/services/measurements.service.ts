//Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
//Interface
import { MeasurementInterface } from '../models/measurement.interaface';
import { UploadPhotoInterface } from '../models/upload-photo.interface';
//other
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MeasurementsService {
  constructor(private http: HttpClient) {}

  addMeasurement(
    measurement: MeasurementInterface
  ): Observable<MeasurementInterface> {
    return this.http.post<MeasurementInterface>(
      `${environment.apiUrl}/measurements`,
      measurement
    );
  }

  getMeasurements(): Observable<MeasurementInterface[]> {
    return this.http.get<MeasurementInterface[]>(
      `${environment.apiUrl}/measurements`
    );
  }

  updateMeasurement(
    id: string,
    measurement: MeasurementInterface
  ): Observable<MeasurementInterface> {
    return this.http.patch<MeasurementInterface>(
      `${environment.apiUrl}/measurements/${id}`,
      measurement
    );
  }
  deleteMeasurement(id: string): Observable<MeasurementInterface> {
    return this.http.delete<MeasurementInterface>(
      `${environment.apiUrl}/measurements/${id}`
    );
  }

  uploadPhoto(formData: FormData): Observable<UploadPhotoInterface> {
    return this.http.post<UploadPhotoInterface>(
      `${environment.apiUrl}/image-upload`,
      formData
    );
  }

  deletePhoto(id: string): Observable<{ result: string }> {
    return this.http.delete<{ result: string }>(
      `${environment.apiUrl}/image-upload/delete/${id}`
    );
  }
}
