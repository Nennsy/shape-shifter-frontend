//Angular
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { concatMap, Observable, pipe, Subject, takeUntil } from 'rxjs';
import { Router } from '@angular/router';
//Services
import { MeasurementsService } from './services/measurements.service';
//Interface
import { MeasurementInterface } from './models/measurement.interaface';
//Components
import { CreateMeasurementComponent } from './create-measurement/create-measurement.component';
import { MeasurementPhotoComponent } from './measurement-photo/measurement-photo/measurement-photo.component';

@Component({
  selector: 'app-measurements',
  templateUrl: './measurements.component.html',
  styleUrls: ['./measurements.component.scss'],
})
export class MeasurementsComponent implements OnInit {
  measurements: MeasurementInterface[] | null = null;
  dialogRef!: MatDialogRef<CreateMeasurementComponent>;
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private measurementService: MeasurementsService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getMeasurements();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  backToHomePage(): void {
    this.router.navigate(['/home']);
  }

  deleteMeasurement(measurementID: string) {
    const measurement = this.measurements?.find((e) => e._id === measurementID);
    if (measurement) {
      this.measurementService
        .deletePhoto(measurement.photoId)
        .pipe(
          concatMap(() =>
            this.measurementService.deleteMeasurement(measurementID)
          ), takeUntil(this.destroy$)
        )
        .subscribe(() => this.getMeasurements());
    }
  }

  getMeasurements(): void {
    this.measurementService
      .getMeasurements()
      .pipe(takeUntil(this.destroy$))
      .subscribe((msh) => {
        this.measurements = msh.sort((a, b) => {
          const d1 = new Date(a.date);
          const d2 = new Date(b.date);
          return d2.getTime() - d1.getTime();
        });
      });
  }

  openMeasurementEditDialog(measurementID?: string): void {
    const measurement = this.measurements?.find((e) => e._id === measurementID);
    this.dialogRef = this.dialog.open(CreateMeasurementComponent, {
      data: measurement,
    });
    this.dialogRef.componentInstance.onUpdateMeasurement
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.getMeasurements();
      });
  }

  openPhoto(photoUrl: string): void {
    this.dialog.open(MeasurementPhotoComponent, { data: { src: photoUrl } });
  }
}
