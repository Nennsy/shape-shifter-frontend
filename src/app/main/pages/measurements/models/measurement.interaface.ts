export interface MeasurementInterface {
    _id: string;
    photoId: string;
    photoUrl: string;
    weight: number;
    chest: number;
    waist: number;
    hips: number;
    biceps: number;
    date: string;
    user: string;
}
