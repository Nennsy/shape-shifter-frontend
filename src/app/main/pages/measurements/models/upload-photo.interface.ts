export interface UploadPhotoInterface {
  url: string;
  public_id: string;
}
