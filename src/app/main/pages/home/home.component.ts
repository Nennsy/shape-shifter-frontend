//Angular
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
//Services
import { AuthService } from '../../../core/services/auth.service';
//Interface
import { UserInterface } from '../../../core/models/users.interface';
//Components
import { ProfileEditorComponent } from './profile-editor/profile-editor.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  user!: UserInterface | null;
  dialogRef!: MatDialogRef<ProfileEditorComponent>;
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private authService: AuthService, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.user = this.authService.getUser();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  openProfileEditor(): void {
    this.dialogRef = this.dialog.open(ProfileEditorComponent);
    this.dialogRef.componentInstance.onUpdateUser
      .pipe(takeUntil(this.destroy$))
      .subscribe((user) => {
        this.user = user;
        this.dialogRef.close();
      });
  }
}
