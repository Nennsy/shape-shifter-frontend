import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';
import { passwordValidator } from '../../../../auth/utils/password-validator';
//Services
import { AuthService } from '../../../../core/services/auth.service';
import { UserService } from '../../../../core/services/user.service';
//Interface
import { UserInterface } from '../../../../core/models/users.interface';
//Components
import { HomeComponent } from '../home.component';

@Component({
  selector: 'app-profile-editor',
  templateUrl: './profile-editor.component.html',
  styleUrls: ['./profile-editor.component.scss'],
})
export class ProfileEditorComponent implements OnInit, OnDestroy {
  @Output() onUpdateUser: EventEmitter<UserInterface> = new EventEmitter();

  profileForm!: FormGroup;
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<HomeComponent>
  ) {}

  ngOnInit(): void {
    const user = this.authService.getUser();
    this.profileForm = this.fb.group({
      name: [user?.name, [Validators.required]],
      email: [user?.email, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(6)]],
      confirmPassword: [null, [Validators.required, passwordValidator]],
      gender: [user?.gender, [Validators.required]],
      dateOfBirth: [user?.dateOfBirth, [Validators.required]],
      height: [user?.height, [Validators.required]],
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  closeModal(flag: boolean): void {
    this.dialogRef.close(flag);
  }

  submitUserUpdate(): void {
    if (this.profileForm.invalid) {
      this.profileForm.markAllAsTouched();
      return;
    }
    this.userService
      .edit(this.profileForm.value)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        this.onUpdateUser.emit(res);
      });
  }
}
