//Angular
import { CommonModule } from '@angular/common';
//Module
import { NgModule } from '@angular/core';
import { MainRoutingModule } from './main-routing.module';
import { LayoutModule } from '../layout/layout.module';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

//Components
import { MainComponent } from './main.component';
import { HomeComponent } from './pages/home/home.component';
import { AdminPanelComponent } from './pages/admin-panel/admin-panel.component';
import { ExercisesComponent } from './pages/exercises/exercises.component';
import { WorkoutsComponent } from './pages/workouts/workouts.component';
import { MeasurementsComponent } from './pages/measurements/measurements.component';
import { ProfileEditorComponent } from './pages/home/profile-editor/profile-editor.component';
import { CreateMeasurementComponent } from './pages/measurements/create-measurement/create-measurement.component';
import { MeasurementPhotoComponent } from './pages/measurements/measurement-photo/measurement-photo/measurement-photo.component';
//Other
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
  declarations: [
    MainComponent,
    HomeComponent,
    AdminPanelComponent,
    ExercisesComponent,
    WorkoutsComponent,
    MeasurementsComponent,
    ProfileEditorComponent,
    CreateMeasurementComponent,
    MeasurementPhotoComponent,
  ],

  imports: [
    CommonModule,
    MainRoutingModule,
    MatDialogModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MainModule {}
