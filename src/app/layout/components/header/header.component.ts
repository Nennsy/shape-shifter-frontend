//Angular
import { Component } from '@angular/core';
import { Router } from '@angular/router';
//Service
import { AuthService } from '../../../core/services/auth.service';
//other
import { LocalStorageUtil } from '../../../core/utils/local-storage.utils';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  constructor(
    private authService: AuthService,
    private router: Router,
    private localStorageUtil: LocalStorageUtil
  ) {}

  onLogout(): void {
    this.router.navigate(['/landing-page']);
    this.localStorageUtil.removeAccessToken();
  }

  isBtnDisplayed(): boolean {
    return this.authService.isAdmin();
  }
}
