import { AbstractControl } from '@angular/forms';

export function passwordValidator(
  control: AbstractControl
): { [key: string]: boolean } | null  {
  const confirmPassword = control;
  const password = control.parent?.get('password');

  return password && confirmPassword && password.value !== confirmPassword.value
    ? { 'mis-match': true }
    : null;
};
