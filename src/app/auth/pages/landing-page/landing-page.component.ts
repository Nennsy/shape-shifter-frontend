// angular
import { OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
//Components
import { Component } from '@angular/core';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent implements OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(public dialog: MatDialog) {}

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  openLoginDialog(): void {
    this.dialog.open(LoginComponent);
  }

  openRegisterDialog(): void {
    this.dialog
      .open(RegistrationComponent)
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe((e) => e && this.openLoginDialog());
  }
}
