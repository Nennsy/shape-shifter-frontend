//Angular
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
//Module
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { SharedModule } from '../shared/shared.module';
//Components
import { LoginComponent } from './pages/landing-page/components/login/login.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { RegistrationComponent } from './pages/landing-page/components/registration/registration.component';

@NgModule({
  declarations: [LoginComponent, LandingPageComponent, RegistrationComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    MatDialogModule,
    ReactiveFormsModule,
    SharedModule,
  ],
})
export class AuthModule {}
