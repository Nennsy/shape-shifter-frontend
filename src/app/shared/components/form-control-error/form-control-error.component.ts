import { Component, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-form-control-error',
  templateUrl: './form-control-error.component.html',
  styleUrls: ['./form-control-error.component.scss'],
})
export class FormControlErrorComponent {
  @Input('form-control')
  formControl!: AbstractControl | null;

  getErrorStatus(type: string): boolean {
    return (
      !!this.formControl &&
      (this.formControl.dirty || this.formControl.touched) &&
      this.formControl.errors?.[type]
    );
  }
}
