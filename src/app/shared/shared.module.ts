import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControlErrorComponent } from './components/form-control-error/form-control-error.component';


@NgModule({
  declarations: [
    FormControlErrorComponent
  ],
  imports: [CommonModule],
  exports: [FormControlErrorComponent],
})
export class SharedModule {}
