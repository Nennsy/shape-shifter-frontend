//Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, tap } from 'rxjs';
//Interface
import { ExistingUserInterface } from 'src/app/auth/models/existinguser.interface';
import { TokenResponse } from '../models/token.interface';
//other
import { environment } from '../../../environments/environment';
import { LocalStorageUtil } from '../utils/local-storage.utils';
import { UserInterface } from '../models/users.interface';
import { decodeToken } from '../utils/token-decoder';
import { Role } from '../enums/role.enum';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private localStorageUtil: LocalStorageUtil
  ) {}

  login(user: ExistingUserInterface): Observable<TokenResponse> {
    return this.http
      .post<TokenResponse>(`${environment.apiUrl}/auth/login`, user)
      .pipe(
        tap(
          (response) =>
            response && this.localStorageUtil.setAccessToken(response.token)
        )
      );
  }

  register(user: UserInterface): Observable<UserInterface> {
    return this.http.post<UserInterface>(
      `${environment.apiUrl}/auth/register`,
      user
    );
  }

  isAuthenticated(): boolean {
    const token = this.localStorageUtil.getAccessToken();
    return !!(token && token.length);
  }

  getUser(): UserInterface | null {
    if (!this.isAuthenticated()) return null;
    const tokenData = decodeToken(this.localStorageUtil.getAccessToken());
    return tokenData && tokenData.user;
  }

  isAdmin(): boolean {
    const user = this.getUser();
    if (!user) return false;
    if (user.role !== Role.Admin) return false;
    return true;
  }
}
