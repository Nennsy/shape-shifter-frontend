import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
//Services
import { AuthService } from './auth.service';
//Interface
import { UserInterface } from '../models/users.interface';
//other
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private authService: AuthService, private http: HttpClient) {}

  edit(user: UserInterface): Observable<UserInterface> {
    const currentUser = this.authService.getUser() as UserInterface;
    
    return this.http.patch<UserInterface>(
      `${environment.apiUrl}/users/${currentUser.id}`,
      user
    );
  }
}
