import { Role } from '../enums/role.enum';

export interface UserInterface {
  id?: string;
  name: string;
  email: string;
  password?: string;
  gender: string;
  dateOfBirth: Date;
  height: number;
  role?: Role;
}

export default {};
