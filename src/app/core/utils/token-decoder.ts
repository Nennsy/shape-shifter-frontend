import jwt_decode from 'jwt-decode';
import { UserInterface } from '../models/users.interface';

export function decodeToken(token: string): { user: UserInterface } | null {
  try {
    return jwt_decode(token);
  } catch (Error) {
    return null;
  }
}
