import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class LocalStorageUtil {
  private tokenStorageKey = 'access-token';

  getAccessToken(): string {
    return localStorage.getItem(this.tokenStorageKey) || '';
  }

  setAccessToken(accessToken: string): void {
    localStorage.setItem(this.tokenStorageKey, accessToken);
  }

  removeAccessToken(): void {
    localStorage.removeItem(this.tokenStorageKey);
  }
}
