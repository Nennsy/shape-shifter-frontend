//Angular
import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild } from '@angular/router';
import { Router } from '@angular/router';
//Services
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(): boolean {
    const authenticated = this.authService.isAuthenticated();

    if (authenticated) return true;

    this.router.navigate(['/landing-page']);
    return false;
  }

  canActivateChild(): boolean {
    return this.canActivate();
  }
}
