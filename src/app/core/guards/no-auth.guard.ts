//Angular
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
//Services
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class NoAuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(): boolean {
    const authenticated = this.authService.isAuthenticated();
    if (authenticated) {
      this.router.navigate(['/home']);
      return false;
    }
    return true;
  }
}
